const myList = document.querySelector(".list-container");

const mapArrayToPage = arr => {
  arr.map(el => {
    let node = document.createElement("LI");
    let textnode = document.createTextNode(`${el}`);
    node.appendChild(textnode);
    myList.appendChild(node);
  });
};

const myArr = ["hello", "world", "Baku", "IBA Tech Academy", "2019"];
const list = mapArrayToPage(myArr);
 