function operate(num1, num2, operation) {
  return operation === "+"
    ? num1 + num2
    : operation === "-"
    ? num1 - num2
    : operation === "*"
    ? num1 * num2
    : operation === "/"
    ? num1 / num2
    : null;
}

const a = parseInt(prompt("Enter your first number"));
const b = parseInt(prompt("Enter your second number"));
const operation = prompt(
  "Enter your operation that wil operate your calculation (Note: you can select only one of these  +, -, *, / operations.)"
);

const result = operate(a, b, operation);

console.log(result);